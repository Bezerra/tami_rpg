(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 550,
	height: 400,
	fps: 24,
	color: "#FFFFFF",
	manifest: []
};



// symbols:



(lib.botao1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AjXANICNjpIEJA+IAZEPIj7Brg");
	this.shape.setTransform(21.6,22.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,43.3,44.2);


// stage content:
(lib.teste = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		hex.addEventListener(MouseEvent.CLICK, inc);
		
		function inc(e:MouseEvent):void
		{
		    var count:int = test.text--;
		    test.text = String(count);
			go
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Camada 1
	this.test = new cjs.Text("10", "bold 18px 'Times New Roman'");
	this.test.name = "test";
	this.test.lineHeight = 22;
	this.test.lineWidth = 173;
	this.test.setTransform(107.1,42);

	this.hex = new lib.botao1();
	this.hex.setTransform(46.4,118.6,1,1,0,0,0,21.6,22.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("AwYmoMAgxAAAIAANRMggxAAAg");
	this.shape.setTransform(205,63.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AwYGpIAAtRMAgxAAAIAANRg");
	this.shape_1.setTransform(205,63.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.hex},{t:this.test}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(299.8,220,286.2,120.7);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;